## [7.2.3](https://gitlab.com/to-be-continuous/s3/compare/7.2.2...7.2.3) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([7331a70](https://gitlab.com/to-be-continuous/s3/commit/7331a709980ba2e136ce2b76c1bac840d66ed8cf))

## [7.2.2](https://gitlab.com/to-be-continuous/s3/compare/7.2.1...7.2.2) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([eec17a3](https://gitlab.com/to-be-continuous/s3/commit/eec17a35aaf2b70b53be77ee973f79a4962772f6))

## [7.2.1](https://gitlab.com/to-be-continuous/s3/compare/7.2.0...7.2.1) (2024-1-30)


### Bug Fixes

* sanitize empty variable test expressions ([d86eb48](https://gitlab.com/to-be-continuous/s3/commit/d86eb48cc9d14093ffdb59e1502bfc23afd7af50))

# [7.2.0](https://gitlab.com/to-be-continuous/s3/compare/7.1.0...7.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([3123652](https://gitlab.com/to-be-continuous/s3/commit/3123652e18929f8e7924e66adb9726af2e49b8b0))

# [7.1.0](https://gitlab.com/to-be-continuous/s3/compare/7.0.1...7.1.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([604b602](https://gitlab.com/to-be-continuous/s3/commit/604b602c53c04cdd86b9979a4daca7625dc559ca))

## [7.0.1](https://gitlab.com/to-be-continuous/s3/compare/7.0.0...7.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([d33223f](https://gitlab.com/to-be-continuous/s3/commit/d33223f80f5f5122b65aa1b83d9b4e55790597ea))

# [7.0.0](https://gitlab.com/to-be-continuous/s3/compare/6.0.0...7.0.0) (2023-09-26)


* feat!: support environment auto-stop ([80fc31b](https://gitlab.com/to-be-continuous/s3/commit/80fc31b29d1097162628037f802a67537cc0787a))


### BREAKING CHANGES

* now review environments will auto stop after 4 hours
by default. Configurable (see doc).

# [6.0.0](https://gitlab.com/to-be-continuous/s3/compare/5.2.0...6.0.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires explicit configuration (see doc) ([133ecbf](https://gitlab.com/to-be-continuous/s3/commit/133ecbfb940b5d612a41855dfac2099d9eb13a0f))


### BREAKING CHANGES

* **oidc:** OIDC authentication support now requires explicit configuration (see doc)

# [5.2.0](https://gitlab.com/to-be-continuous/s3/compare/5.1.0...5.2.0) (2023-06-26)


### Features

* promote (pseudo-standard) $environment_type & $environment_name variables ([83bf653](https://gitlab.com/to-be-continuous/s3/commit/83bf653fe3fd531be62e27a9a33b1b6aee6cd83d))

# [5.1.0](https://gitlab.com/to-be-continuous/s3/compare/5.0.0...5.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([1163e99](https://gitlab.com/to-be-continuous/s3/commit/1163e99fa1f1c5f12ffb7171f4d2f6658a9594be))

# [5.0.0](https://gitlab.com/to-be-continuous/s3/compare/4.1.1...5.0.0) (2023-04-05)


### Features

* **deploy:** redesign deployment strategy ([320f7bc](https://gitlab.com/to-be-continuous/s3/commit/320f7bcd3af0a9307c902daa5ca0104cd90a557d))


### BREAKING CHANGES

* **deploy:** $AUTODEPLOY_TO_PROD no longer supported (replaced by $S3_PROD_DEPLOY_STRATEGY - see doc)

## [4.1.1](https://gitlab.com/to-be-continuous/s3/compare/4.1.0...4.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([00c2ddb](https://gitlab.com/to-be-continuous/s3/commit/00c2ddb3738f3b0eb70012b5a1a850c6d693383c))

# [4.1.0](https://gitlab.com/to-be-continuous/s3/compare/4.0.0...4.1.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider image ([ee13901](https://gitlab.com/to-be-continuous/s3/commit/ee13901f22bc50e32c8fdb0a7d2312f3cd707470))

# [4.0.0](https://gitlab.com/to-be-continuous/s3/compare/3.0.0...4.0.0) (2022-12-01)


### Bug Fixes

* document S3_DEPLOY_ARGS variable ([122c090](https://gitlab.com/to-be-continuous/s3/commit/122c09019f3190ec6c0d8221dc69499272fefbec))
* leave stderr logs on loging ([c115bfe](https://gitlab.com/to-be-continuous/s3/commit/c115bfe6ea2ed3d67002f7fb7c1932709d228cbc))
* normalize prefix ([3341dd1](https://gitlab.com/to-be-continuous/s3/commit/3341dd158e67df27ef8f33b5e225bc62c6c3cfa9))


### Features

* change default deploy args (sync instead of put) ([11a7335](https://gitlab.com/to-be-continuous/s3/commit/11a7335351b09aebf1404021c22f56df8d8e7971))
* change default website create args ([e51a9d9](https://gitlab.com/to-be-continuous/s3/commit/e51a9d96fc5426dfe5b854f2958ef28e4f7c16db))
* multi S3 providers support ([6a12e39](https://gitlab.com/to-be-continuous/s3/commit/6a12e394a9bb92cba63b1b39b0df2a62873ca0ee))
* shared bucket support ([255906e](https://gitlab.com/to-be-continuous/s3/commit/255906e0e110091c5ad07511d3b30843e23b005a))


### BREAKING CHANGES

* - S3_ROOT_PATH replaced with S3_PREFIX
- S3_REGION is now required to enable bucket creation

# [3.0.0](https://gitlab.com/to-be-continuous/s3/compare/2.3.0...3.0.0) (2022-08-05)


### Features

* make MR pipeline the default workflow ([4f4f788](https://gitlab.com/to-be-continuous/s3/commit/4f4f788ceb0d4f5c72908c08b000f375e5d82e2a))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.3.0](https://gitlab.com/to-be-continuous/s3/compare/2.2.0...2.3.0) (2022-06-30)


### Features

* enforce AUTODEPLOY_TO_PROD and PUBLISH_ON_PROD as boolean variables ([74ac203](https://gitlab.com/to-be-continuous/s3/commit/74ac203bb27292ee364560834ae1539752e51b01))

# [2.2.0](https://gitlab.com/to-be-continuous/s3/compare/2.1.5...2.2.0) (2022-05-01)


### Features

* configurable tracking image ([6d7380d](https://gitlab.com/to-be-continuous/s3/commit/6d7380d7b3d33d77332caf9431e306496dfee55c))

## [2.1.5](https://gitlab.com/to-be-continuous/s3/compare/2.1.4...2.1.5) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([fa1f8a1](https://gitlab.com/to-be-continuous/s3/commit/fa1f8a1ded6e3c1a10bc692ffb6e7910070f5c2b))

## [2.1.4](https://gitlab.com/to-be-continuous/s3/compare/2.1.3...2.1.4) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([02ee218](https://gitlab.com/to-be-continuous/s3/commit/02ee218db568b1265347ea17ef7928ac69f75c4d))

## [2.1.3](https://gitlab.com/to-be-continuous/s3/compare/2.1.2...2.1.3) (2022-01-10)


### Bug Fixes

* non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([9a73a37](https://gitlab.com/to-be-continuous/s3/commit/9a73a377c961ed9c34062b7e40391c2c6663c22e))

## [2.1.2](https://gitlab.com/to-be-continuous/s3/compare/2.1.1...2.1.2) (2021-12-03)


### Bug Fixes

* execute hook scripts with shebang shell ([c388355](https://gitlab.com/to-be-continuous/s3/commit/c3883551936382e11969610c488bac978048d4cf))

## [2.1.1](https://gitlab.com/to-be-continuous/s3/compare/2.1.0...2.1.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([5fdc76e](https://gitlab.com/to-be-continuous/s3/commit/5fdc76e944f5708b73ae9aa1bd47578faa838de2))

## [2.1.0](https://gitlab.com/to-be-continuous/s3/compare/2.0.0...2.1.0) (2021-09-16)

### Features

* add possibility to add a bucket prefix at deploy ([191c88c](https://gitlab.com/to-be-continuous/s3/commit/191c88c7ed9572ecb5dee808c6c3ccaa51dfcf2f))

## [2.0.0](https://gitlab.com/to-be-continuous/s3/compare/1.2.1...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([ab37f24](https://gitlab.com/to-be-continuous/s3/commit/ab37f24140322b61b99b4ce619455a1f5ad295fa))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.1](https://gitlab.com/to-be-continuous/s3/compare/1.2.0...1.2.1) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([235dd64](https://gitlab.com/to-be-continuous/s3/commit/235dd640d26083a0cb3cb374588714f31160f60b))

## [1.2.0](https://gitlab.com/to-be-continuous/s3/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([92c02e6](https://gitlab.com/to-be-continuous/s3/commit/92c02e6d146a444c9785b648c5d0a7314e1450ae))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/s3/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([850b2a6](https://gitlab.com/Orange-OpenSource/tbc/s3/commit/850b2a648ee4efe3e5301582bc04a1777ea13c07))

## 1.0.0 (2021-05-06)

### Features

* initial release ([1c00ef6](https://gitlab.com/Orange-OpenSource/tbc/s3/commit/1c00ef6ad2522c5346f97ad98850a5883b1d3012))
