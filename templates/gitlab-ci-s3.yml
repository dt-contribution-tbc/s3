# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms 
# of the GNU Lesser General Public License as published by the Free Software Foundation; 
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this 
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
# default workflow rules: Merge Request pipelines
spec:
  inputs:
    cmd-image:
      description: The Docker image used to run [s3cmd](https://s3tools.org/usage) commands
      default: registry.hub.docker.com/d3fk/s3cmd:latest
    endpoint-host:
      description: Default S3 endpoint hostname (with port)
      default: s3.amazonaws.com
    host-bucket:
      description: Default DNS-style bucket+hostname:port template for accessing a bucket
      default: '%(bucket)s.$S3_ENDPOINT_HOST'
    region:
      description: Default region to create the buckets in (if not defined, the template won't create any)
      default: ''
    base-bucket-name:
      description: Base bucket name
      default: $CI_PROJECT_NAME
    deploy-args:
      description: '[s3cmd](https://s3tools.org/usage) command and options to deploy files to the bucket'
      default: sync --recursive --delete-removed --acl-public --no-mime-magic --guess-mime-type
    deploy-files:
      description: Pattern(s) of files to deploy to the S3 bucket
      default: public/
    website-disabled:
      description: Disables WebSite hosting by your S3 bucket
      type: boolean
      default: false
    website-endpoint:
      description: Default WebSite endpoint url pattern (supports `%(bucket)s` and `%(location)s` placeholders)
      default: http://%(bucket)s.s3-website.%(location)s.amazonaws.com
    website-args:
      description: '[s3cmd](https://s3tools.org/usage) command and options to enable WebSite hosting on the bucket'
      default: ws-create --ws-index=index.html --ws-error=404.html
    prefix:
      description: Default S3 prefix to use as a root destination to upload objects in the S3 bucket
      default: ''
    scripts-dir:
      description: Directory where S3 hook scripts are located
      default: .
    review-disabled:
      description: Disable Review
      type: boolean
      default: false
    review-endpoint-host:
      description: S3 endpoint hostname (with port) for `review` env  _(only define to override default)_
      default: ''
    review-region:
      description: Region to create the `review` buckets in (if not defined, the template won't create any)
      default: ''
    review-bucket-name:
      description: Bucket name for `review` env _(only define to override default)_
      default: ''
    cleanup-all-review:
      description: |-
        Enables a **manual** job to cleanup all review envs at once.

        You may also use it to [schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) cloud resources cleanup. See documentation.
      type: boolean
      default: false
    review-prefix:
      description: S3 prefix to use for `review` env _(only define to override default)_
      default: ''
    review-autostop-duration:
      description: The amount of time before GitLab will automatically stop `review` environments
      default: 4 hours
    integ-disabled:
      description: Disable Integration
      type: boolean
      default: false
    integ-endpoint-host:
      description: S3 endpoint hostname (with port) for `integration` env  _(only define to override default)_
      default: ''
    integ-region:
      description: Region to create the `integration` bucket in
      default: ''
    integ-bucket-name:
      description: Bucket name for `integration` env _(only define to override default)_
      default: ''
    integ-prefix:
      description: S3 prefix to use for `integration` env _(only define to override default)_
      default: ''
    staging-disabled:
      description: Disable Staging
      type: boolean
      default: false
    staging-endpoint-host:
      description: S3 endpoint hostname (with port) for `staging` env  _(only define to override default)_
      default: ''
    staging-region:
      description: Region to create the `staging` bucket in
      default: ''
    staging-bucket-name:
      description: Bucket name for `staging` env _(only define to override default)_
      default: ''
    staging-prefix:
      description: S3 prefix to use for `staging` env _(only define to override default)_
      default: ''
    prod-disabled:
      description: Disable Production
      type: boolean
      default: false
    prod-endpoint-host:
      description: S3 endpoint hostname (with port) for `production` env  _(only define to override default)_
      default: ''
    prod-region:
      description: Region to create the `production` bucket in
      default: ''
    prod-bucket-name:
      description: Bucket name for `production` env _(only define to override default)_
      default: ''
    prod-deploy-strategy:
      description: Defines the deployment to production strategy.
      options:
      - manual
      - auto
      default: manual
    prod-prefix:
      description: S3 prefix to use for `production` env _(only define to override default)_
      default: ''
---
workflow:
  rules:
    # prevent MR pipeline originating from production or integration branch(es)
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $PROD_REF || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $INTEG_REF'
      when: never
    # on non-prod, non-integration branches: prefer MR pipeline over branch pipeline
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*tag(,[^],]*)*\]/" && $CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*branch(,[^],]*)*\]/" && $CI_COMMIT_BRANCH'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*mr(,[^],]*)*\]/" && $CI_MERGE_REQUEST_ID'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*default(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*prod(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $PROD_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*integ(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $INTEG_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*dev(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    - when: always

variables:
  # variabilized tracking image
  TBC_TRACKING_IMAGE: registry.gitlab.com/to-be-continuous/tools/tracking:master

  S3_CMD_IMAGE: $[[ inputs.cmd-image ]]
  # defaults to AWS
  S3_ENDPOINT_HOST: $[[ inputs.endpoint-host ]]
  S3_HOST_BUCKET: $[[ inputs.host-bucket ]]
  S3_BASE_BUCKET_NAME: $[[ inputs.base-bucket-name ]]
  S3_SCRIPTS_DIR: $[[ inputs.scripts-dir ]]
  S3_BASE_TEMPLATE_NAME: s3
  S3_REVIEW_AUTOSTOP_DURATION: $[[ inputs.review-autostop-duration ]]
  S3_DEPLOY_ARGS: $[[ inputs.deploy-args ]]
  S3_DEPLOY_FILES: $[[ inputs.deploy-files ]]
  S3_WEBSITE_ARGS: $[[ inputs.website-args ]]
  # defaults to AWS
  S3_WEBSITE_ENDPOINT: $[[ inputs.website-endpoint ]]

  # default: one-click deploy
  S3_PROD_DEPLOY_STRATEGY: $[[ inputs.prod-deploy-strategy ]]

  # default production ref name (pattern)
  PROD_REF: /^(master|main)$/
  # default integration ref name (pattern)
  INTEG_REF: /^develop$/

  S3_REGION: $[[ inputs.region ]]
  S3_WEBSITE_DISABLED: $[[ inputs.website-disabled ]]
  S3_PREFIX: $[[ inputs.prefix ]]
  S3_REVIEW_DISABLED: $[[ inputs.review-disabled ]]
  S3_REVIEW_ENDPOINT_HOST: $[[ inputs.review-endpoint-host ]]
  S3_REVIEW_REGION: $[[ inputs.review-region ]]
  S3_REVIEW_BUCKET_NAME: $[[ inputs.review-bucket-name ]]
  CLEANUP_ALL_REVIEW: $[[ inputs.cleanup-all-review ]]
  S3_REVIEW_PREFIX: $[[ inputs.review-prefix ]]
  S3_INTEG_DISABLED: $[[ inputs.integ-disabled ]]
  S3_INTEG_ENDPOINT_HOST: $[[ inputs.integ-endpoint-host ]]
  S3_INTEG_REGION: $[[ inputs.integ-region ]]
  S3_INTEG_BUCKET_NAME: $[[ inputs.integ-bucket-name ]]
  S3_INTEG_PREFIX: $[[ inputs.integ-prefix ]]
  S3_STAGING_DISABLED: $[[ inputs.staging-disabled ]]
  S3_STAGING_ENDPOINT_HOST: $[[ inputs.staging-endpoint-host ]]
  S3_STAGING_REGION: $[[ inputs.staging-region ]]
  S3_STAGING_BUCKET_NAME: $[[ inputs.staging-bucket-name ]]
  S3_STAGING_PREFIX: $[[ inputs.staging-prefix ]]
  S3_PROD_DISABLED: $[[ inputs.prod-disabled ]]
  S3_PROD_ENDPOINT_HOST: $[[ inputs.prod-endpoint-host ]]
  S3_PROD_REGION: $[[ inputs.prod-region ]]
  S3_PROD_BUCKET_NAME: $[[ inputs.prod-bucket-name ]]
  S3_PROD_PREFIX: $[[ inputs.prod-prefix ]]

stages:
  - build
  - test
  - package-build
  - package-test
  - infra
  - deploy
  - acceptance
  - publish
  - infra-prod
  - production

.s3-scripts: &s3-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
    echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
    echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
    echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function fail() {
    log_error "$*"
    exit 1
  }

  function assert_defined() {
    if [[ -z "$1" ]]
    then
      log_error "$2"
      exit 1
    fi
  }

  function install_ca_certs() {
    certs=$1
    if [[ -z "$certs" ]]
    then
      return
    fi

    # import in system
    if echo "$certs" >> /etc/ssl/certs/ca-certificates.crt
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/certs/ca-certificates.crt\\e[0m"
    fi
    if echo "$certs" >> /etc/ssl/cert.pem
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/cert.pem\\e[0m"
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue; 
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue; 
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue; 
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue; 
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue; 
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue; 
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue; 
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  # evaluate and export a secret
  # - $1: secret variable name
  function eval_secret() {
    name=$1
    value=$(eval echo "\$${name}")
    case "$value" in
    @b64@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | base64 -d > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded base64 secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding base64 secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @hex@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | sed 's/\([0-9A-F]\{2\}\)/\\\\x\1/gI' | xargs printf > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded hexadecimal secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding hexadecimal secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @url@*)
      url=$(echo "$value" | cut -c6-)
      if command -v curl > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if curl -s -S -f --connect-timeout 5 -o "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully curl'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      elif command -v wget > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if wget -T 5 -O "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully wget'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      else
        log_warn "Couldn't get secret \\e[33;1m${name}\\e[0m: no http client found"
      fi
      ;;
    esac
  }

  function eval_all_secrets() {
    encoded_vars=$(env | grep -Ev '(^|.*_ENV_)scoped__' | awk -F '=' '/^[a-zA-Z0-9_]*=@(b64|hex|url)@/ {print $1}')
    for var in $encoded_vars
    do
      eval_secret "$var"
    done
  }
  
  # determines whether passed host matches one of the patterns from $no_proxy
  function matches_no_proxy() {
    # shellcheck disable=SC2154
    no_proxy_ic=${no_proxy:-$NO_PROXY}
    for pattern in ${no_proxy_ic//,/ }
    do
      if echo "$1" | grep -q -e "$pattern\$"
      then
        return 0
      fi
    done
    return 1
  }

  function s3_login() {
    endpoint_host=${ENV_ENDPOINT_HOST:-$S3_ENDPOINT_HOST}
    host_bucket=${S3_HOST_BUCKET}
    access_key=${ENV_ACCESS_KEY:-$S3_ACCESS_KEY}
    secret_key=${ENV_SECRET_KEY:-$S3_SECRET_KEY}

    assert_defined "$endpoint_host" 'Missing required S3 endpoint host'
    # shellcheck disable=SC2016
    assert_defined "$host_bucket" 'Missing required env $S3_HOST_BUCKET'
    assert_defined "$access_key" 'Missing required S3 access key'
    assert_defined "$secret_key" 'Missing required S3 secret key'

    echo -e "[default]\\nhost_base = $endpoint_host\\nhost_bucket = $host_bucket\\nwebsite_endpoint = $S3_WEBSITE_ENDPOINT\\naccess_key = $access_key\\nsecret_key = $secret_key" > ~/.s3cfg

    # shellcheck disable=SC2154
    proxy_ic=${https_proxy:-$HTTPS_PROXY}
    proxy_hostport=${proxy_ic#*//}
    if [[ "$proxy_hostport" ]] && ! matches_no_proxy "$endpoint_host"
    then
      log_info "... Proxy \\e[33;1m$proxy_hostport\\e[0m has to be used to reach \\e[33;1m$endpoint_host\\e[0m"
      echo -e "proxy_host = ${proxy_hostport%:*}\\nproxy_port = ${proxy_hostport#*:}" >> ~/.s3cfg
    fi

    if s3cmd ls >/dev/null
    then
      log_info "... Login to API endpoint \\e[33;1msuccedeed\\e[0m"
    else
      fail "... Login to API endpoint \\e[33;1mfailed\\e[0m"
    fi
  }

  # upload/sync files to bucket
  function s3_deploy() {
    export environment_type=$ENV_TYPE
    bucket=${ENV_BUCKET_NAME:-${S3_BASE_BUCKET_NAME}${ENV_APP_SUFFIX}}
    prefix=${ENV_PREFIX:-${S3_PREFIX}}
    region=${ENV_REGION:-${S3_REGION}}
    # normalize prefix with leading slash
    if [[ "$prefix" ]] && [[ "${prefix:0:1}" != "/" ]]
    then
      prefix="/$prefix"
    fi
    # backward compatibility
    export env=$environment_type

    log_info "--- \\e[32mdeploy\\e[0m"
    log_info "--- \$environment_type: \\e[33;1m${environment_type}\\e[0m"
    log_info "--- \$bucket: \\e[33;1m${bucket}\\e[0m"
    log_info "--- \$region: \\e[33;1m${region}\\e[0m"
    log_info "--- \$prefix: \\e[33;1m${prefix:-(none)}\\e[0m"

    # unset any upstream deployment env & artifacts
    rm -f s3.env
    rm -f environment_url.txt

    # maybe create bucket
    if s3cmd info "s3://${bucket}" >/dev/null 2>&1
    then
      log_info "... bucket \\e[33;1m${bucket}\\e[0m found: continue"
    elif [[ "$region" ]]
    then
      log_info "... bucket \\e[33;1m${bucket}\\e[0m not found: create (in \\e[33;1m${region}\\e[0m)"
      # shellcheck disable=SC2086
      s3cmd ${TRACE+-v} mb --region "$region" "s3://${bucket}"
    else
      log_error "... bucket \\e[33;1m${bucket}\\e[0m not found and region not specified: fail"
      log_error "... either create it or set the region to enable auto-create (see doc)"
      exit 1
    fi

    # maybe execute pre deploy script
    prescript="$S3_SCRIPTS_DIR/s3-pre-deploy.sh"
    if [[ -f "$prescript" ]]
    then
      log_info "--- \\e[32mpre-deploy hook\\e[0m (\\e[33;1m${prescript}\\e[0m) found: execute"
      chmod +x "$prescript"
      "$prescript"
    else
      log_info "--- \\e[32mpre-deploy hook\\e[0m (\\e[33;1m${prescript}\\e[0m) not found: skip"
    fi

    # sync files
    log_info "... synchronize files"
    # shellcheck disable=SC2086
    s3cmd ${TRACE+-v} $S3_DEPLOY_ARGS $S3_DEPLOY_FILES "s3://${bucket}${prefix}/"

    # create website
    if [[ "$S3_WEBSITE_DISABLED" != "true" ]]
    then
      log_info "... create website"
      # shellcheck disable=SC2086
      s3cmd ${TRACE+-v} $S3_WEBSITE_ARGS "s3://${bucket}"
      # obtain website url
      website_url=$(s3cmd ws-info "s3://${bucket}" | grep -i "website endpoint" | cut -d':' -f2- | tr -d '[:space:]')
      # remote trailing slash
      website_url=${website_url%/}
      # build environment url (with prefix)
      environment_url="$website_url$prefix"
      # finally persist environment url
      echo "$environment_url" > environment_url.txt
      echo -e "environment_type=$environment_type\\nenvironment_name=$bucket\\nenvironment_url=$environment_url" > s3.env
    fi
  }

  # delete application
  function s3_delete() {
    export environment_type=$ENV_TYPE
    bucket=${ENV_BUCKET_NAME:-${S3_BASE_BUCKET_NAME}${ENV_APP_SUFFIX}}
    prefix=${ENV_PREFIX:-${S3_PREFIX}}
    # normalize prefix with leading slash
    if [[ "$prefix" ]] && [[ "${prefix:0:1}" != "/" ]]
    then
      prefix="/$prefix"
    fi

    log_info "--- \\e[32mdelete\\e"
    log_info "--- \$environment_type: \\e[33;1m${environment_type}\\e[0m"
    log_info "--- bucket: \\e[33;1m${bucket}\\e[0m"
    log_info "--- prefix: \\e[33;1m${prefix:-(none)}\\e[0m"

    # check if bucket exists
    if ! s3cmd info "s3://${bucket}" >/dev/null
    then
      log_info "... bucket \\e[33;1m${bucket}\\e[0m not found: skip"
      return
    fi

    # delete files
    log_info "... delete files from \\e[33;1m${bucket}${prefix}\\e[0m"
    s3cmd ${TRACE+-v} rm --recursive --force "s3://${bucket}${prefix}/"

    # finally delete bucket if empty
    objs_count=$(s3cmd du "s3://${bucket}" | tr -s ' ' | cut -d' ' -f3)
    if [[ "$objs_count" == "0" ]]
    then
      log_info "... bucket is now empty: try to delete"
      if ! s3cmd ${TRACE+-v} rb "s3://${bucket}"
      then
        log_warn "... delete \\e[33;1m${bucket}\\e[0m failed"
      fi
    fi
  }

  function s3_delete_all() {
    export environment_type=$1
    envnameproto=$2
    prefix=$3

    # make environment_name regex by replacing $CI_COMMIT_REF_SLUG with .*
    envnameregex=$(echo "$envnameproto" | sed -r "s/$CI_COMMIT_REF_SLUG/.*/g")
    # list buckets | extract 3rd col | strip 's3://' | filter buckets with "$regex"
    matchingbuckets=$(s3cmd ls | awk '{print $3}' | cut -d'/' -f3 | awk "/$envnameregex/")
    matchcount=$(echo "$matchingbuckets" | wc -w)

    log_info "--- \\e[32mdelete all"
    log_info "--- \$environment_type: \\e[33;1m${environment_type}\\e[0m"
    log_info "--- bucket matcher: \\e[33;1m${envnameregex}\\e[0m (\\e[33;1m${matchcount}\\e[0m buckets found)"
    log_info "--- prefix: \\e[33;1m${prefix:-(none)}\\e[0m"

    rc=0
    for bckt in $matchingbuckets
    do
      echo -e "\\e[1;93m-------------------------------------------------------------------------------\\e[0m"
      if ! delete "$environment_type" "$bckt" "$prefix"
      then
        log_warn "... failed deleting bucket \\e[33;1m${bckt}\\e[0m (see logs)"
        rc=1
      fi
    done
    return $rc
  }

  unscope_variables
  eval_all_secrets

  # ENDSCRIPT

# job prototype
# defines default Docker image, tracking probe, cache policy and tags
#
# @arg ENV_ENDPOINT_HOST: env-specific S3 endpoint host
# @arg ENV_ACCESS_KEY   : env-specific S3 access key
# @arg ENV_SECRET_KEY   : env-specific S3 secret key
.s3-base:
  image:
    name: $S3_CMD_IMAGE
    entrypoint: [""]
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "s3", "7.2.3"]
  before_script:
    - !reference [.s3-scripts]
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - s3_login

# Deploy job prototype
# Can be extended to define a concrete environment
#
# @arg ENV_TYPE         : environment type
# @arg ENV_BUCKET_NAME  : env-specific S3 bucket name
# @arg ENV_REGION       : env-specific region
# @arg ENV_PREFIX       : env-specific S3 prefix
# @arg ENV_APP_SUFFIX   : env-specific application suffix
.s3-deploy:
  extends: .s3-base
  stage: deploy
  variables:
    ENV_APP_SUFFIX: "-$CI_ENVIRONMENT_SLUG"
  script:
    - s3_deploy
  artifacts:
    name: "$ENV_TYPE env url for $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - environment_url.txt
    reports:
      dotenv: s3.env
  environment:
    url: "$environment_url" # can be either static or dynamic

# Cleanup job prototype
# Can be extended for each deletable environment
#
# @arg ENV_TYPE         : environment type
# @arg ENV_BUCKET_NAME  : env-specific S3 bucket name
# @arg ENV_PREFIX       : env-specific S3 prefix
# @arg ENV_APP_SUFFIX   : env-specific application suffix
.s3-cleanup:
  extends: .s3-base
  stage: deploy
  # force no dependencies
  dependencies: []
  # no need to clone repository
  variables:
    GIT_STRATEGY: none
    ENV_APP_SUFFIX: "-$CI_ENVIRONMENT_SLUG"
  script:
    - s3_delete
  environment:
    action: stop

# deploy to review env (only for feature branches)
s3-review:
  extends: .s3-deploy
  variables:
    ENV_TYPE: review
    ENV_BUCKET_NAME: "$S3_REVIEW_BUCKET_NAME"
    ENV_REGION: "$S3_REVIEW_REGION"
    ENV_PREFIX: "$S3_REVIEW_PREFIX"
    ENV_ENDPOINT_HOST: "$S3_REVIEW_ENDPOINT_HOST"
    ENV_ACCESS_KEY: "$S3_REVIEW_ACCESS_KEY"
    ENV_SECRET_KEY: "$S3_REVIEW_SECRET_KEY"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    on_stop: s3-cleanup-review
    auto_stop_in: "$S3_REVIEW_AUTOSTOP_DURATION"
  resource_group: review/$CI_COMMIT_REF_NAME
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # exclude if $S3_REVIEW_DISABLED set
    - if: '$S3_REVIEW_DISABLED == "true"'
      when: never
    # exclude if $CLEANUP_ALL_REVIEW set to 'force'
    - if: '$CLEANUP_ALL_REVIEW == "force"'
      when: never
    # only on non-production, non-integration branches
    - if: '$CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'

# stop review env (automatically triggered once branches are deleted)
s3-cleanup-review:
  extends: .s3-cleanup
  variables:
    ENV_TYPE: review
    ENV_BUCKET_NAME: "$S3_REVIEW_BUCKET_NAME"
    ENV_PREFIX: "$S3_REVIEW_PREFIX"
    ENV_ENDPOINT_HOST: "$S3_REVIEW_ENDPOINT_HOST"
    ENV_ACCESS_KEY: "$S3_REVIEW_ACCESS_KEY"
    ENV_SECRET_KEY: "$S3_REVIEW_SECRET_KEY"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  resource_group: review/$CI_COMMIT_REF_NAME
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # exclude if $S3_REVIEW_DISABLED set
    - if: '$S3_REVIEW_DISABLED == "true"'
      when: never
    # exclude if $CLEANUP_ALL_REVIEW set to 'force'
    - if: '$CLEANUP_ALL_REVIEW == "force"'
      when: never
    # only on non-production, non-integration branches
    - if: '$CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: manual
      allow_failure: true

# stop all review envs (manual job on master branch)
s3-cleanup-all-review:
  extends: .s3-base
  stage: deploy
  # force no dependencies
  dependencies: []
  variables:
    # no need to clone repository
    GIT_STRATEGY: none
    ENV_TYPE: review
    ENV_ENDPOINT_HOST: "$S3_REVIEW_ENDPOINT_HOST"
    ENV_ACCESS_KEY: "$S3_REVIEW_ACCESS_KEY"
    ENV_SECRET_KEY: "$S3_REVIEW_SECRET_KEY"
  script:
    - s3_delete_all review "${S3_REVIEW_BUCKET_NAME:-${S3_BASE_BUCKET_NAME}-review-.*}" "${S3_REVIEW_PREFIX:-${S3_PREFIX}}"
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # exclude if $S3_REVIEW_DISABLED set
    - if: '$S3_REVIEW_DISABLED == "true"'
      when: never
    # on any branch with $CLEANUP_ALL_REVIEW set to 'force': auto, always (doesn't depend on upstream pipeline succeeds)
    - if: '$CLEANUP_ALL_REVIEW == "force"'
      when: always
      allow_failure: true
    # on production: manual
    - if: '$CLEANUP_ALL_REVIEW == "true" && $CI_COMMIT_REF_NAME =~ $PROD_REF'
      when: manual
      allow_failure: true

# deploy to integration env (for 'develop' branch by default)
s3-integration:
  extends: .s3-deploy
  variables:
    ENV_TYPE: integration
    ENV_BUCKET_NAME: "$S3_INTEG_BUCKET_NAME"
    ENV_REGION: "$S3_INTEG_REGION"
    ENV_PREFIX: "$S3_INTEG_PREFIX"
    ENV_ENDPOINT_HOST: "$S3_INTEG_ENDPOINT_HOST"
    ENV_ACCESS_KEY: "$S3_INTEG_ACCESS_KEY"
    ENV_SECRET_KEY: "$S3_INTEG_SECRET_KEY"
  environment:
    name: integration
  resource_group: integration
  rules:
    # exclude if $S3_INTEG_DISABLED set
    - if: '$S3_INTEG_DISABLED == "true"'
      when: never
    # only on integration branch(es)
    - if: '$CI_COMMIT_REF_NAME =~ $INTEG_REF'

# deploy to staging env (for 'master' branch by default)
# the staging env is meant for testing and validation purpose
s3-staging:
  extends: .s3-deploy
  variables:
    ENV_TYPE: staging
    ENV_BUCKET_NAME: "$S3_STAGING_BUCKET_NAME"
    ENV_REGION: "$S3_STAGING_REGION"
    ENV_PREFIX: "$S3_STAGING_PREFIX"
    ENV_ENDPOINT_HOST: "$S3_STAGING_ENDPOINT_HOST"
    ENV_ACCESS_KEY: "$S3_STAGING_ACCESS_KEY"
    ENV_SECRET_KEY: "$S3_STAGING_SECRET_KEY"
  environment:
    name: staging
  resource_group: staging
  rules:
    # exclude if $S3_STAGING_DISABLED set
    - if: '$S3_STAGING_DISABLED == "true"'
      when: never
    # only on production branch(es)
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF'

# deploy to production env
s3-production:
  extends: .s3-deploy
  stage: production
  variables:
    ENV_TYPE: production
    ENV_APP_SUFFIX: "" # no suffix for prod
    ENV_BUCKET_NAME: "$S3_PROD_BUCKET_NAME"
    ENV_REGION: "$S3_PROD_REGION"
    ENV_PREFIX: "$S3_PROD_PREFIX"
    ENV_ENDPOINT_HOST: "$S3_PROD_ENDPOINT_HOST"
    ENV_ACCESS_KEY: "$S3_PROD_ACCESS_KEY"
    ENV_SECRET_KEY: "$S3_PROD_SECRET_KEY"
  environment:
    name: production
  resource_group: production
  rules:
    # exclude non-production branches
    - if: '$CI_COMMIT_REF_NAME !~ $PROD_REF'
      when: never
    # exclude if $S3_PROD_DISABLED set
    - if: '$S3_PROD_DISABLED == "true"'
      when: never
    - if: '$S3_PROD_DEPLOY_STRATEGY == "manual"'
      when: manual
    - if: '$S3_PROD_DEPLOY_STRATEGY == "auto"'
